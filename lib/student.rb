class Student

  attr_reader :first_name, :last_name

  attr_accessor :courses

  def initialize(first_name, last_name)

    @first_name = first_name
    @last_name = last_name
    @courses = []

  end

  def name
    first_name + ' ' + last_name
  end

  def enroll(course)

    if has_conflict?(course)
      raise 'This course conflicts with an already enrolled course'
    end

    if !courses.include?(course)
      courses.push(course)
      course.students.push(self)
    end
  end

  def course_load

    course_count = Hash.new(0)

    courses.each do |course|
      course_count[course.department] += course.credits
    end
    course_count
  end

  def has_conflict?(new_course)
    courses.each do |course|
      if course.conflicts_with?(new_course)
        return true
      end
    end
    false
  end

end
